// Bài 1
const NUMBER = 10000;

document.getElementById("btnShowResult").onclick = function(){
    var sum = 0;
    for( i = 0; sum < NUMBER; i++ ) {
        sum += i;
        if(sum > NUMBER)
            break;
    }
    
    document.getElementById("result").innerHTML = `Số nguyên dương nhỏ nhất là: ${i}`;
}

// Bài 2

// Bài 3
document.getElementById("btnFactorial").onclick = function(){
    // input
    var number = document.getElementById("importNumber").value * 1;

    // output
    var factorial = 1;
    for(i = 1; i <= number ; i++) {
        factorial *= i;
    }

    document.getElementById("result_3").innerHTML = `Giai Thừa: ${factorial}`;
}

// Bài 4
document.getElementById("createDiv").onclick = function() {
    var content = '';
    for(n = 1; n <= 10; n++){
        if(n % 2 == 0) {
           var div = ` <div class="alert alert-warning bg-danger">Div chẵn</div>`;
           content += div;
        } else {
            var div = `<div class="alert alert-warning bg-primary">Div lẻ</div>`;
            content += div;
        }
    }
    document.getElementById("result_4").innerHTML = content;
}
